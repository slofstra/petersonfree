def combine_exp(a,b):
    out = []
    difference_found = false
    i=0
    j=0

    # loop through and check all differences
    while i<len(a) and j<len(b):
        if a[i]==b[j]:
            out.append(a[i])
            i=i+1
            j=j+1
        else:
            if not difference_found:
                difference_found=true
                out.append(a[i]+1)
                i=i+1
            else:
                return false

    # of course, the difference might come at the end
    if i<len(a) and not difference_found:
        difference_found=true
        out.append(a[i]+1)
        i = i+1

    # at this point, we should be done, and we should have found a difference
    if i<len(a) or j<len(b) or not difference_found:
        return false

    return out

# M is a list of vectors in a vector space V.
# C is a certificate for inductive freeness: 
#   - C is a list containing either 0 or 3 elements
#   - In the first case, M must be all zero vectors
#   - In the second case, C = (n, C1, C2), where:
#       - V(M[n]) is non-zero
#       - C1 = certificate for M \ M[n] 
#       - C2 = certificate for M / M[n]
# Here M \ M[n] is the deletion of M[n] from M. The deletion is modeled by
# setting M[n]=V(0), so that column numbering is not changed.
# M / M[n] is the quotient of M by M[n], defined by replacing V with
# W = V.quotient([M[n]]), and each element M[i] with W(M[i]).
#
# Returns: the list of exponents of M calculated from certificate C, or
# false if the certificate fails.
def calculate_exponents(M, V, C):
    if len(C) == 3:
        #print "Pivoting"
        #print(C[0])
        #print(M)
        # make sure M[n] is non-zero
        if M[C[0]]==V(0):
            print "Failure: pivot is zero"
            print C[0]
            return false

        # do the restriction and calculate exponents
        W = V.quotient([M[C[0]]])
        Er = calculate_exponents(map(W, M), W, C[2])
        if Er == false:
            #print "Failure: on restriction"
            return false

        # set up the deletion, and calculate exponents
        Md = copy(M)
        # delete every vector colinear to M[C[0]]
        # check colinearity by seeing if vector is zero in W
        for i in range(0, len(Md)):
            if W(Md[i])==W(0):
                Md[i] = V(0)

        Ed = calculate_exponents(Md, V, C[1])
        if Ed == false:
            #print "Failure: on deletion"
            return false


        return combine_exp(Ed, Er)
    
    elif len(C) == 0:
        # get the non-zero vectors
        NZ = filter(lambda v : v <> V(0), M)
        if len(NZ) != 0:

            # we still have pivots, but at this point every possible pivot must work
            P = NZ[0]

            # do the restriction and calculate exponents
            W = V.quotient([P])
            Er = calculate_exponents(map(W, M), W, [])
            if Er == false:
                #print "Failure: on restriction"
                return false

            # set up the deletion, and calculate exponents
            Md = copy(M)
            # delete every vector colinear to M[P]
            # check colinearity by seeing if vector is zero in W
            for i in range(0, len(Md)):
                if W(Md[i])==W(0):
                    Md[i] = V(0)

            Ed = calculate_exponents(Md, V, [])
            if Ed == false:
                #print "Failure: on deletion"
                return false


            return combine_exp(Ed, Er)

        else: 
            #print "Reached bottom successfully"
            return [0]*V.dimension()

    else:
        print "Failure: C is not correctly formatted"
        return false

# A starter function for calculate_exponents. 
# Takes a list of lists, along with a certificate as above,
# and constructs the ambient vector space and vectors.
def check_certificate(M,C):
    V = VectorSpace(QQ, len(M[0]))
    return calculate_exponents(map(V, M), V, C)

