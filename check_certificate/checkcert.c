#include <stdio.h>
#include <gmp.h>
#include <stdlib.h>

#define DEBUG 0
#define DEBUG_PRINT(...) do { if(DEBUG) gmp_printf(__VA_ARGS__); } while(0);

// Convention: A must always be set to a matrix, with m and n as dimensions
#define M(A,i,j) *((A)+(i)*n+(j))


// copy matrix src to matrix target
void copy_matrix(mpq_t *src, mpq_t *target, int m, int n)
{
    int i;
    for (i=0; i<m*n; i++) {
        mpq_init(target[i]);
        mpq_set(target[i], src[i]);
    }
}

// free the memory associated with a matrix
void delete_matrix (mpq_t *A, int m, int n)
{
    int i;
    for (i=0; i<m*n; i++) {
        mpq_clear(A[i]);
    }
    free(A);
}

// return true if the jth column is zero, and false otherwise
int col_is_zero (int j, mpq_t *A, int m, int n)
{
    int i;
    for (i=0; i<m; i++) {
        if (mpq_sgn(M(A,i,j)) != 0) {
            return 0;
        }
    }

    return 1;
}

// print out the matrix
void print_matrix (mpq_t *A, int m, int n)
{
    int i,j;
    for (i=0; i<m; i++) {
        for (j=0; j<n; j++) {
            gmp_printf("%Qd ", M(A,i,j));
        }
        putchar('\n');
    }
}

// do column operations to reduce from column j, and then set j to zero
void quotient_by_column(int j, mpq_t *A, int m, int n)
{
    int i, p, k;
    mpq_t t, s;

    
    // find a pivot to clear the column
    for (p=0; p<m && mpq_sgn(M(A,p,j))==0; p++) ;

    if (p==m) {
        fprintf(stderr, "No pivot in %dth column.\n", j);
        exit(1);
    }

    DEBUG_PRINT("Pivot row is %d\n", p);

    // scale the pivot to 1 
    mpq_init(s);
    mpq_set(s, M(A,p,j));
    for (i=0; i<m; i++) 
        mpq_div(M(A,i,j), M(A,i,j), s);

    // do column operations, clear out pth row in other columns
    mpq_init(t);
    for (k=0; k<n; k++) {
        if (k != j) {
            //DEBUG_PRINT("Column %d\n", k);
            mpq_set(s, M(A,p,k));
            for (i=0; i<m; i++) {
                mpq_mul(t, s, M(A,i,j));
                //DEBUG_PRINT("\t %Qd, %Qd, %Qd, %Qd\n", M(A,i,k), t, M(A,p,k), M(A,i,j));
                mpq_sub(M(A,i,k), M(A,i,k), t);
            }
        }
    }

    // now set the jth column to zero
    for (i=0; i<m; i++) {
        mpq_set_ui(M(A,i,j),0,1);
    }

    // free our helper variables
    mpq_clear(s);
    mpq_clear(t);
}    

// combined exponents are stored in d
int combine_exp(int *d, int dl, int *r, int rl)
{
    int i=0, j=0, df=0;
    while (i<dl || j<rl) {
        if (i<dl && j<rl && d[i]==r[j]) {
            i++;
            j++;
            
        }
        else if (i<dl && !df) {
            df=1;
            d[i]++;
            i++;
        }
        else {
            // exponents can't combine
            return 0;
        }
    }

    // we were successful if both i and j are at their limits
    return i==dl && j==rl;
}

void print_exp(int *e, int m)
{
    int i;
    printf("Exponents are ");
    for (i=0; i<m; i++) {
        printf("%d ", e[i]);
    }
    putchar('\n');
}

// A should not be changed
int *calc_exp(mpq_t *A, int m, int n, int d, FILE *cert)
{
    int c, j=0, i, s, *de, *re;
    mpq_t *D, *R;

    DEBUG_PRINT("Entering calc_exp, dimension is %d\n", d);

    // STAGE 1: clear out the front matter and figure out
    // what state we are in. If cert is non-null, we want to
    // use cert for a hint. Otherwise we are in end-bracket
    // mode, meaning we take the first non-zero column as a
    // pivot.
    if (cert != NULL) {
        fgetc(cert);
        c = fgetc(cert);
    }
    else {
        c = ']';
    }

    // STAGE 2: get a pivot. In end-bracket mode, we look for
    // a non-zero column as a pivot. If we fail to get a pivot, set j=n. If we
    // aren't in end-bracket mode, then we always get a pivot from the
    // certificate.

    // End-bracket mode
    if (c==']') {
        DEBUG_PRINT("Hit end-bracket mode\n");

        // turn off the certificate
        cert = NULL;

        // try and find a non-zero column
        for (j=0; j<n && col_is_zero(j,A,m,n); j++) ;
    }
    // Not in end-bracket mode.
    else {
        // c should be the first digit of j, so get the remaining digits
        while (c != ',') {
            DEBUG_PRINT("Read char c=%c, j is now %d.\n", c, j);
            j = 10*j + (((char) c) - '0');
            c = fgetc(cert);
        }
    }

    if (j == n) {
        // if we failed to find a pivot, then we return exponent 0 with
        // multiplicity equal to d.
        de = malloc(sizeof(int) * d);
        for (i=0; i<d; i++) de[i]=0;
    }
    // otherwise we pivot
    else {
        DEBUG_PRINT("Pivoting, column is %d, matrix is:\n", j);
        if (DEBUG) print_matrix(A, m, n);

        // Prepare to recurse, set up deletion and restriction matrices
        D = malloc(sizeof(mpq_t) * m * n);
        R = malloc(sizeof(mpq_t) * m * n);
        copy_matrix(A,R,m,n);
        copy_matrix(A,D,m,n);

        // Calculate the restriction
        quotient_by_column(j, R, m, n);
        DEBUG_PRINT("Restriction matrix:\n");
        if (DEBUG) print_matrix(R, m, n);
        
        // Note: we don't need j any more
        // If the jth column of R is zero, then make the jth column
        // of D zero.
        for (j=0; j<n; j++) {
            if (col_is_zero(j,R,m,n)) {
                for (i=0; i<m; i++) {
                    mpq_set_ui(M(D,i,j), 0, 1);
                }
            }
        }
        DEBUG_PRINT("Deletion matrix:\n");
        if (DEBUG) print_matrix(D, m, n);

        // calculate the exponents for the deletion
        de=calc_exp(D, m, n, d, cert);

        // if we aren't in end-bracket mode, clear the next comma
        if (cert != NULL) fgetc(cert);

        // calculate the exponents for the restriction
        re=calc_exp(R, m, n, d-1, cert);
        
        DEBUG_PRINT("Going to combine exponents.");
        if(DEBUG) print_exp(de,d);
        if(DEBUG) print_exp(re,d-1); 
        // combine the exponents, free the temporary variables, and return
        if (!combine_exp(de, d, re, d-1)) {
            fprintf(stderr, "Calculation of exponents failed, certificate invalid.\n");
            exit(1);
        }
        delete_matrix(D, m, n);
        delete_matrix(R, m, n);
        free(re);

        // if we aren't in end-bracket mode, clear our end-bracket 
        if (cert != NULL) fgetc(cert);
    }
    DEBUG_PRINT("Final exponents ready:\n");
    if(DEBUG) print_exp(de,d);
    return de;
}

int main(int argc, char *argv[])
{
    FILE *cert;
    int m,n,i,j, v, *e;
    mpq_t *A;
    
    if (argc==2 && (cert=fopen(argv[1], "r")) != NULL) {
        scanf("%d", &m);
        scanf("%d", &n);
        A = malloc(sizeof(mpq_t) * m * n);
        for (i=0; i<m; i++) {
            for (j=0; j<n; j++) {
                scanf("%d", &v);
                mpq_init(M(A,i,j));
                mpq_set_si(M(A,i,j), v, 1);
            }
        }
        e = calc_exp(A, m, n, m, cert);
        print_exp(e, m);
        fclose(cert);
    }
    else {
        printf("Usage: checkcert cert_file_name\n");
    }
    return 0;
}
