
## See if I can come up with a faster NullSpace, since the one in
## LinearAlgebra is incredibly slow.

## If M is sparse, (set storage=sparse in the Matrix constructor)
## then FastNullSpace can be even faster. 

FastNullSpace := proc(M)
local w,i,out,assign,n,j,ws;

    w := LinearSolve(M,Vector(RowDimension(M)),free='s');
    i := 1; ws := w; n := 0;
    while has(ws,'s') do
        if has(w,'s'[i]) then
            n := n+1;
            w := subs({'s'[i]='s'[n]},w);
            ws := subs({'s'[i]=0},ws);
        fi;    
        i := i+1 
    od;

    out := NULL;
    for i from 1 to n do
        assign := NULL;
        for j from 1 to n do 
            assign := assign, 's'[j] = `if`(i=j,1,0)
        od;
        out := out, subs({assign},w);
    od;
    {out}
end:

    
