# Peterson translates.
#
# Contents:
# 1. Peterson translates at the identity.
# - ptranslate
#
# 2. Reverse Peterson translate (at the identity)
# - reverse_ptranslate
#
# 3. Dual Peterson translates
# - co_ptranslate
# - double_ptranslate

##############################################################################
# 1. Peterson translates at the identity
# This Peterson translate is the geometric Peterson translate, assuming all
# weights are conjugated to the identity (before and after the translation).

# Peterson translation at the identity. 
# Simply compresses a-strings, no reflection involved. 
# R is a list of the positive roots.
ptranslate := proc(a,S,R)
local Sout, v, c; 
    Sout := NULL;
    for v in S do
        c := 0;
        while member(v,R) do
            if member(v,S) then c := c+1 fi;
            v := v-a;
        od;
        Sout := Sout, (v + c*a);
    od;
    [Sout];
end:

##############################################################################
# 2. Reverse Peterson translates (at the identity)

# expand a-strings as much as possible, instead of contracting them
reverse_ptranslate := proc(a,S,R)
local Sout, v, c;
    Sout := NULL;
    for v in S do
        c := 0;
        while member(v,R) do
            if member(v,S) then c := c+1 fi;
            v := v+a;
        od;
        Sout := Sout, (v - c*a);
    od;
    [Sout];
end:

###############################################################################
# 3. Dual Peterson translate at the identity.
# Change to the coroot system, apply Peterson translation, and then return to
# the original system.

co_ptranslate := (a,S,R) -> list_coroots(ptranslate(coroot(a),list_coroots(S), list_coroots(R))):

double_ptranslate := (a,S,R) -> co_ptranslate(a,ptranslate(a,S,R), R):

###############################################################################
# 4. Peterson graphs
# The Peterson graph is a list of the form [...,[n,C,L,...],...] where in each
# entry C is a coconvex set, n is an integer, and L is a list of integers
# corresponding to the Peterson translates of C. Other properties can be listed
# after this depending on application.

# Construct the Peterson graph for root system R
ptgraph := proc(R)
local CC, PR, CCout, E, v, i, j, PT;
    PR := pos_roots(R);
    CC := select(is_coconvex, combinat[powerset](PR), R, PR);
    CCout := NULL;
    
    for i from 1 to nops(CC) do
        E := NULL;
        for v in CC[i] do
            PT := ptranslate(v, CC[i], PR);
            if not set_eq(PT, CC[i]) then
                j := set_find(PT, CC);
                if not member(j, [E]) then 
                    E := E, j;
                fi
            fi
        od;
        CCout := CCout, [i, list_root_coords(CC[i], R), sort([E])];
    od;
    [CCout]
end:

# Change the entry S in the Peterson graph to [op(S), f(S)]
graph_add_prop := (f, PTG) -> map(S -> [op(S), f(S)], PTG):

# Let P be a property of a coconvex set. We say that S is Peterson-P if
# S is P and every Peterson translate of S is P. Suppose that PTG is a
# Peterson graph, where the last element of every entry is a boolean
# corresponding to the property P. This function adds the property Peterson-P
# to the end of each entry.
graph_pt_propagate := PTG -> 
    graph_add_prop(S -> `and`(S[nops(S)], op(map(i -> PTG[i, nops(S)], S[3]))), PTG):

# We say that a coconvex set is Peterson free if every Peterson translate of it
# is free, and k-Peterson free if it is free, and we cannot reach a non-free coconvex
# set within k Peterson translations. 
# This function returns the Peterson graph for root system R, with added properties
# corresponding to freeness = 0-Peterson freeness, 1-Peterson freeness, 2-Peterson freeness,
# ..., k-Peterson freeness. 
free_pt_graph := proc(R, k := 0)
local i, P;
    P := graph_add_prop(S -> ind_free(list_to_mat(S[2])), ptgraph(R));
    for i from 1 to k do P := graph_pt_propagate(P) od;
    P
end:

# Takes a Peterson graph P and a variable number of booleans b1, ..., bk.
# The Peterson graph must have at least k properties attached to each entry.
# Returns the entries for which the first k properties match b1,...,bk.
pt_graph_filter := proc(PTG)
local L, fil, n;
    L := [_rest];
    n := nops(L);
    fil := proc(S)
        local i;
        for i from 1 to n do
            if S[3+i] <> L[i] then return false fi
        od;
        true
    end;
    select(fil, PTG)
end:
