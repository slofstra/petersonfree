##############################################################################
This directory contains functions for verifying facts from the paper I want
to write about inversion hyperplane arrangements and freeness. 

This is the second version of the program. The first version worked with convex
sets closed under positive integers. This version works with convex sets
pairwise closed under positive rationals.


##############################################################################
Data conventions:

Root systems use the conventions from John Stembridge's coxeter package, with
    the exception that Cartan matrices follow the convention from Kumar (the
    transpose of Stembridge's convention.

Matroids are represented by matrices, with elements given by columns.

#############################################################################
Files:

hyp.M
    Hypothesis testing - finished versions.

free.M
    Freeness and formality.

rootsystems.M
    Functions for working with root systems.

peterson.M
    Peterson translates. 

util.M
    Utility functions for lists and I/O.

nullspace.M
    A numerical version of nullspace, used for speed. 
