#include <vector>
#include <iostream>
#include <list>

class Polynomial {
        /********** Private data memebers **********/
        std::vector<long long> v;
        int d;

        /********** Helper methods **********/
        bool splits_helper(std::list<unsigned> &E, int);

    public:
        
        /********** Constructors **********/
        Polynomial () : v(0,0), d(-1) {} // zero

        /********** Overloaded operations ***********/
        Polynomial & operator+= (const Polynomial &);
        Polynomial & operator-= (const Polynomial &);
        Polynomial & operator*= (const Polynomial &);
        Polynomial & operator+= (const long long);
        Polynomial & operator-= (const long long);
        Polynomial & operator*= (const long long);

        friend std::ostream& operator<<(std::ostream&, const Polynomial&);

        /********** Static factory functions **********/
        static Polynomial monomial(unsigned d); 
        static Polynomial unit() { return monomial(0); } 
        static Polynomial zero() { return Polynomial(); }

        /********** Special polynomial functions **********/

        // replace the variable x with a*x
        Polynomial & subs(const long long a);

        // multiply by x, shifting all terms by 1
        Polynomial & shift(unsigned n); 
        Polynomial & shift() { return shift(1); } 

        // reverse the coefficients of the polynomial
        Polynomial & reverse();

        // See if the polynomial splits into linear factors with all non-negative
        // roots. If so, return the roots in E. If the polynomial doesn't split,k
        // then E should be unchanged.
        bool splits(std::list<unsigned> &E);

        int degree() { return d; }

};

Polynomial operator+ (const Polynomial &, const Polynomial &);
Polynomial operator- (const Polynomial &, const Polynomial &);
Polynomial operator* (const Polynomial &, const Polynomial &);
Polynomial operator+ (const long long, const Polynomial &);
Polynomial operator- (const long long, const Polynomial &);
Polynomial operator* (const long long, const Polynomial &);
Polynomial operator+ (const Polynomial &, const long long);
Polynomial operator- (const Polynomial &, const long long);
Polynomial operator* (const Polynomial &, const long long);


