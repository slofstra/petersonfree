
// 1 to print keepalives, 0 to turn them off
#define KEEPALIVE 1

// Set to:
// 0 - debug off
// 1 - debug inductive freeness checking
// 2 - print certificate
// 3 - print minimal non-free elements
#define DEBUG 0

#define DEBUG_PRINT(c,x) do { if(DEBUG==c) { print_tab(); cout << x << endl; } } while(0)
#define DEBUG_PUSH do { if (DEBUG) debug_tab++; } while(0)
#define DEBUG_POP do { if (DEBUG) debug_tab--; } while(0)

// A function which prints a period to the screen to track progress
void print_keepalive();
void print_tab();
extern unsigned debug_tab;
