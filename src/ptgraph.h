/* Construct the Peterson translation graph of a root system. 
   This is the directed graph with vertex set equal to the coconvex sets, and edges
   from each coconvex set to all its Peterson translates. 
   The data structure also allows associating different properties, such as freeness,
   to each coconvex set. 

   Ultimately the Peterson graph can be used to calculate Peterson freeness. We say that
   a coconvex set C is 0-Peterson free if it is free, and k-Peterson free for k>=1 if it 
   and all it's translates are (k-1)-Peterson free. Equivalently C is k-Peterson free if
   freeness is preserved by up to k-Peterson translations.
*/

#include "rootsys.h"

class PTGraph {
    public:
        /*************** Public types *****************************/

        
        class node {
            public:
                unsigned i; // the index of this node

                // The list of edges. An edge consists of a cbitset denoted a coconvex set,
                // and the column index of a root that can be used to translate from the 
                // current coconvex set to the new coconvex set.
                std::map<Rootsystem::cbitset, unsigned> edges; 

                // k is the degree of freeness that has been checked. 
                // is_free is the result of the last computation. 
                // -1, True indicates that no freeness has been computed.
                // 0, False, Uncertain, True indicates that the result of computing
                //      freeness of the coconvex set. 
                // 1, False, Uncertain, True indicates the result of computing 
                //      1-freeness of the coconvex set.
                // k-freeness of the coconvex set will only be computed if the set
                // is unambiguously (k-1)-free. So if these read k, False or k, Uncertain
                // then the coconvex set must be (k-1)-free with a certainty.
                int k;
                Rootsystem::FuzzyBool is_free;

                node () : k(-1), is_free(Rootsystem::True) {}
                
                // Returns true if it is certain that the coconvex set is j-Peterson free,
                // using the information from k and is_free, and returns false otherwise.
                bool is_pt_free(int j) { return (j<k) || (j==k && (is_free==Rootsystem::True)); }
        };

    private:
        /*************** Private data members *********************/
        Rootsystem R; 
        bool initialized;

        // The graph data consists of two parts: an array of nodes, and a map
        // of coconvex sets to node indexes.
        std::map<Rootsystem::cbitset, node> nodes;
        
       
    public:
        /*************** Constructors *****************************/
        // Takes in a root system. PTGraph will make a copy of the root system,
        // but the root system does not have to be initialized.
        PTGraph (const Rootsystem &Rin) : R(Rin), initialized(0) {}

        // Creates the underlying root system from a matrix. Does not
        // initialize the graph or the underlying root system.
        PTGraph (const matrix & init) : R(init), initialized(0) {}

        /*************** Public functions *************************/

        // Initializes the graph, keeping track of k-Peterson freeness. 
        // If the argument is -1 then no freeness properties are calculated. 
        void initialize_graph(int); 

        // Nothing after this point should be called before initialization!!!

        // Return the number n of nodes in the graph.
        // Nodes are indexed by integers from 0 ... (n-1).
        unsigned size() const { return nodes.size(); }

        // Return the index for a given coconvex set
        // -1 is returned if the set is not coconvex
        int index(const Rootsystem::cbitset &) const;

        /**************** Static utility functions *************/
        static void print (const PTGraph &);
};
