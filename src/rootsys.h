// A Rootsystem is a special kind of Matroid coming from a root system.
// The columns of the defining matrix should be set to the positive
// roots of the root system.

#include "matroid.h"

class Rootsystem : public Matroid
{
    private:
        /********** Private data members **********/
        bool root_sys_initialized; 

        // The (i,j)th entry of this matrix is a number k if i != j, and the
        // ith column of M + the jth column of M is the kth column of M.
        // Otherwise the entry is -1. 
        matrix addition_table;

        std::list<cbitset> convex;
        std::list<cbitset> coconvex;

    public:
        /********** Constructors and initialization **********/
        Rootsystem(const matrix & init) : Matroid(init), root_sys_initialized(0),
            addition_table(init.num_cols(), init.num_cols()) {}
        
        // Computes the addition table, as well as the convex and coconvex sets.
        // Will also initialize the underlying matroid
        void initialize_rootsystem();

        /********** Root system functions **********/
        // Rootsystem should be initialized before using these functions.

        // Get a const reference to the convex and coconvex sets. 
        const std::list<cbitset> &convex_sets() const { return convex; }
        const std::list<cbitset> &coconvex_sets() const { return coconvex; }

        // determine whether or not a set if convex/coconvex/biconvex
        // Note: uses the addition table, so not that fast
        bool is_convex(const cbitset &) const;
        bool is_coconvex(const cbitset &) const; 
        bool is_biconvex(const cbitset &) const;

        /********** Peterson translation **********************/
        cbitset ptranslate(const cbitset &, unsigned) const;
        cbitset inv_ptranslate(const cbitset &, unsigned) const; 

};
