#include "io.h"
using namespace std;

void print_unsigned_list (ostream &os, list<unsigned> &L)
{
    bool first=1;
    for (list<unsigned>::iterator I = L.begin(); I != L.end(); I++) {
        if (!first) {
            os << " " << *I;
        }
        else {
            os << *I;
            first = 0;
        }
    }
}

