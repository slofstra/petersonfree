#include "rootsys.h"
#include "io.h"
#include <iostream>
#include <string>
using namespace std;

void show_help() 
{
    cout << "Usage: indfree command [params]" << endl << endl;
    cout << "where command is one of: cert, sage, inv" << endl;
    cout << "1. indfree cert filename" << endl;
    cout << "Reads in a matrix from standard input, checks to see if it is inductively free," << endl;
    cout << "and if so, writes a certificate file to filename. Status messages are printed to" << endl;
    cout << "standard out." << endl << endl;
    cout << "2. indfree sage filename" << endl;
    cout << "Similar to #1, but prints out a sage readable certificate." << endl << endl;
    cout << "3. indfree inv" << endl;
    cout << "Reads in a matrix from standard input, and prints invariants to standard out." << endl << endl;
}

void certificate(ofstream &os) {
    matrix m = matrix::read();
    matrix::print(m);

    Matroid M(m);
    cout << "Initializing...";
    cout.flush();
    M.initialize();
    cout << "done" << endl;

    cout << "Checking inductive freeness...";
    cout.flush();
    list<unsigned> Ep;
    bool b = M.ind_free(Ep);
    cout << "done" << endl;
    if (b) {
        cout << "Inductively free with exponents ";
        for (list<unsigned>::iterator I = Ep.begin(); I != Ep.end(); I++) {
            cout << *I << " ";
        }
        cout << endl;
        cout << "Generating certificate...";
        cout.flush();
        M.print_certificate(M.empty_minor(), os);
        os << endl;
        os.close();
        cout << "done" << endl;
    }
    else {
        cout << "Not inductively free" << endl;
    }
}

// read in a matrix from standard input, and 
void sage_certificate(ofstream &os) {
    matrix m = matrix::read();
    matrix::print(m);

    Matroid M(m);
    cout << "Initializing...";
    cout.flush();
    M.initialize();
    cout << "done" << endl;

    cout << "Checking inductive freeness...";
    cout.flush();
    list<unsigned> Ep;
    bool b = M.ind_free(Ep);
    cout << "done" << endl;
    if (b) {
        cout << "Inductively free with exponents ";
        for (list<unsigned>::iterator I = Ep.begin(); I != Ep.end(); I++) {
            cout << *I << " ";
        }
        cout << endl;
        cout << "Generating certificate...";
        cout.flush();
        os << "M = [";
        for (int j=0; j<m.num_cols(); j++) {
            os << "[";
            for (int i=0; i<m.num_rows(); i++) {
                os << m(i,j) << ",";
            }
            os << "], ";
        } 
        os << "]" << endl;

        os << "C = ";
        M.print_certificate(M.empty_minor(), os);
        os << endl;
        os.close();
        cout << "done" << endl;
    }
    else {
        cout << "Not inductively free" << endl;
    }
}

// Read in matrix from standard in, print out invariants to standard out
void invariants() 
{
    matrix m = matrix::read();
    matrix::print(m);

    Matroid M(m);
    cout << "Initializing...";
    cout.flush();
    M.initialize();
    cout << "done" << endl;
    cout << "Poincare polynomial: ";
    cout.flush();
    cout << M.poincare_poly() << endl;
    cout << "Characteristic polynomial: ";
    cout.flush();
    cout << M.char_poly() << endl;
    list<unsigned> E;
    if (M.char_poly().splits(E)) {
        cout << "Exponents: ";
        print_unsigned_list(cout, E);
        cout << endl;
    }
    else {
        cout << "Characteristic polynomial does not split." << endl;
    }
    cout << endl << "Flats: " << endl;
    for (int i=0; i<=M.rank(); i++) {
        cout << M.flats(i).size() << " flats of rank " << i << endl;
    }
    cout << endl;
}

int main (int argc, char *argv[])
{
    int mode=0; // mode of operation: -1 == error in parameters, 0 = no mode specified, 
    char *cert_file;
    
    // must be at least one parameter
    if (argc<2) {
        mode=-1;
    }
    else {
        string command = argv[1];
        if (command == "cert") {
            if (argc != 3) {
                cerr << "Error: no certificate file specified." << endl;
                mode = -1;
            }
            else {
                mode=1;
                cert_file = argv[2];
            }
        }
        else if (command == "sage") {
            if (argc != 2) {
                cerr << "Error: no certificate file specified." << endl;
                mode = -1;
            }
            else {
                mode=2;
                cert_file = argv[2];
            }
        }
        else if (command == "inv") {
            if (argc != 2) {
                mode = -1;
            }
            else {
                mode=3;
            }
        }
    }


    if (mode==-1) {
        show_help();
        return 1;
    }
    else if (mode==0) {
        cout << "No mode specified." << endl;
        show_help();
    }
    else if (mode==1) {
        ofstream cert_stream; 
        cert_stream.open(cert_file);
        if (!cert_stream.is_open()) {
            cerr << "Error: couldn't open certificate file." << endl;
        }
        else {
            certificate(cert_stream);
        }
    }
    else if (mode==2) {
        ofstream cert_stream; 
        cert_stream.open(cert_file);
        if (!cert_stream.is_open()) {
            cerr << "Error: couldn't open certificate file." << endl;
        }
        else {
            sage_certificate(cert_stream);
        }
    }
    else if (mode==3) {
        invariants();
    }
}

