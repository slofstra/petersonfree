#include <iostream>
#include <string.h>
#include "matroid_exception.h"
#include "matrix.h"
using namespace std;

/********** Constructors **********/
matrix::matrix (unsigned r, unsigned c) : m(r), n(c) 
{
    a = new long long[m*n];
    memset(a, 0, m*n*sizeof(long long));
}

matrix::matrix (unsigned r, unsigned c, long long *p) : m(r), n(c) 
{
    a = new long long[m*n];
    memcpy(a, p, m*n*sizeof(long long)); 
}

matrix::matrix (const matrix & copy) : m(copy.m), n(copy.n)
{
    a = new long long[m*n];
    memcpy(a, copy.a, m*n*sizeof(long long)); 
}

/********** Operators **********/
matrix & matrix::operator= (const matrix & copy)
{
    m = copy.m;
    n = copy.n;
    delete [] a;
    a = new long long[m*n];
    memcpy(a, copy.a, m*n*sizeof(long long));
}

long long & matrix::operator() (unsigned i, unsigned j)
{
    if (i >= m || j >= n)
        throw str_exception("Index out of range in &matrix::operator()");
    return a[i*n+j]; 
}

long long matrix::operator() (unsigned i, unsigned j) const
{
    if (i >= m || j >= n)
        throw str_exception("Index out of range in matrix::operator()");
    return a[i*n+j];
}

bool matrix::operator== (const matrix & other) const
{
    if (m != other.m || n != other.n) return false;

    for (unsigned i=0; i<m; i++)
        for (unsigned j=0; j<n; j++)
            if (a[i*n+j] != other.a[i*n+j]) return false;

    return true;
}

/********** Private matrix operations **********/

// Uses column j1 to wipe out column j2, using row p as the pivot.
// should never be called with indices out of range, or
// j1 == j2
void matrix::column_op (unsigned j1, unsigned j2, unsigned p)
{
    int x = a[p*n+j1], y = a[p*n+j2];
    // we replace column j2 with (x * column j2) - (y * column j1)
    for (unsigned i=0; i<m; i++)
        a[i*n+j2] = x*a[i*n+j2] - y*a[i*n+j1];
}

// Returns the first non-zero entry of the jth column.
// Returns -1 if the column is zero.
int matrix::column_pivot (unsigned j) const
{
    for (unsigned i=0; i<m; i++)
        if (a[i*n+j] != 0) return i;

    return -1;
}

// Multiplies the jth column by a scalar multiple s.
void matrix::col_mult(unsigned j, long long s)
{
    for (unsigned i=0; i<m; i++)
        a[i*n+j]*=s;
}

// Divides the jth column by a scalar multiple s.
void matrix::col_div(unsigned j, long long s)
{
    for (unsigned i=0; i<m; i++)
        a[i*n+j]/=s;
}

// Multiplies the ith row by a scalar multiple s.
void matrix::row_mult(unsigned i, long long s)
{
    for (unsigned j=0; j<n; j++)
        a[i*n+j]*=s;
}

// Divides the ith row by a scalar multiple s.
void matrix::row_div(unsigned i, long long s)
{
    for (unsigned j=0; j<n; j++)
        a[i*n+j]/=s;
}
/********** Matrix operations **********/

// Clears the pth row using column operations, where p is the pivot of the jth
// column. The jth column is left unchanged. If the jth column is zero, does
// nothing.
void matrix::column_clear (unsigned j)
{
    if (j >= n)
        throw str_exception("Index out of range in matrix::column_clear");

    // get the pivot and check that the column is not zero
    int p = column_pivot(j);
    if (p==-1) return;
    
    // now apply column operations to the columns before j and after j
    for (int j2=0; j2<j; j2++) {
        column_op(j,j2, p);
    }
    for (int j2=j+1; j2<n; j2++) {
        column_op(j,j2, p);
    }
}
        
// Returns true if the column is zero, and false otherwise.
bool matrix::column_is_zero (unsigned j) const
{
    if (j >= n)
        throw str_exception("Index out of range in matrix::column_is_zero");

    return column_pivot(j)==-1;
}

// Returns true if the two columns are parallel, and false otherwise.  Note
// that zero is parallel to any vector.
bool matrix::columns_parallel (unsigned j1, unsigned j2) const
{
    //cout << j1 << ", " << j2 << endl;
    if (j1 >= n || j2 >= n)
        throw str_exception("Index out of range in matrix::columns_parallel");
    
    int p = column_pivot(j1);
    if (p!= -1) {
        // for readability, let x and y be the pth entries of each column 
        int x = a[p*n+j1], y = a[p*n+j2];

        // check that x*a[i][j2] = y*a[i][j1]
        for (int i=0; i<m; i++)
            if (x*a[i*n+j2] != y*a[i*n+j1]) return false;
    }

    return true;
}

// helper function to compute a gcd
long long gcd (long long a, long long b)
{
    if (a < 0) a = -a;
    if (b < 0) b = -b;

    int c;
    while (b != 0) {
        c = b;
        b = a % b;
        a = c;         
    }
    return a;
}
 
void matrix::normalize () 
{
    // normalize each column pivot to be positive
    unsigned i,j;
    long long p;
    for (j=0; j<n; j++) {
        p = column_pivot(j);
        if (p!= -1 && a[p*n+j]<0) col_mult(j,-1);
    }

    // divide the columns by their gcd
    for (j = 0; j<n; j++) {
        p = 0;
        for (i=0; i<m; i++) {
            p = gcd(a[i*n+j], p);
        }
        if (p != 0) col_div(j, p);
    }

    // divide the rows by their gcd
    for (i = 0; i<m; i++) {
        p = 0;
        for (j=0; j<n; j++) {
            p = gcd(a[i*n+j], p);
        }
        if (p != 0) row_div(i, p);
    }
}

/********** Static utility functions **********/

void matrix::print (const matrix &m)
{
    cout << "Current matrix: " << endl;
    for (int i=0; i<m.num_rows(); i++) {
        for (int j=0; j<m.num_cols(); j++)
            cout << m(i,j) << " ";
        cout << endl;
    }
    cout << endl;
}

matrix matrix::read ()
{
    int m,n;
    cin >> m;
    cin >> n;
    matrix M(m,n);
    for (int i=0; i<m; i++)
        for (int j=0; j<n; j++)
            cin >> M(i,j);

    return M;
}

