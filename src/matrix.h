// Matrix class
// In general this class takes care of all the necessary linear algebra (over
// the integers, of course).

class matrix {
    /********** Data members **********/
    unsigned m, n;
    long long *a;

    /********** Private matrix operations **********/
    // These operations don't usually do bounds checking, which
    // is why they should only be called by functions that do 
    // bounds checking.
    void column_op (unsigned, unsigned, unsigned);
    int column_pivot(unsigned) const;
    void col_mult(unsigned, long long);
    void col_div(unsigned, long long);
    void row_mult(unsigned, long long);
    void row_div(unsigned, long long);

    public:
        /********** Constructors **********/
        matrix (unsigned r, unsigned c); // initialize to zero
        matrix (unsigned, unsigned, long long *); // copy array
        matrix (const matrix &); // copy constructor
        ~matrix () { delete[] a; }

        /*********** Operators and access members ***********/
        matrix & operator= (const matrix &);
        long long & operator() (unsigned, unsigned);
        long long operator() (unsigned, unsigned) const;
        bool operator== (const matrix &) const;

        // accessers
        unsigned num_cols() const { return n; }
        unsigned num_rows() const { return m; }

        /********** Matrix operations **********/

        // uses column operations to clear everything in
        // the entry of the column pivot.
        void column_clear(unsigned);

        // Returns true if the column is zero, and false otherwise.
        bool column_is_zero(unsigned) const;

        // Returns true if the two columns are parallel, and false otherwise.
        // Note that zero is parallel to any vector.
        bool columns_parallel(unsigned, unsigned) const;

        // Divide all rows by their gcd, all columns by the gcd, and
        // multiply by +1 or -1 to make every column pivot positive.
        void normalize();

        /********** Static utility functions **********/
        static void print (const matrix &);
        static matrix read ();
};

