#include <exception>
#include <string>
using namespace std;

class str_exception: public exception
{
    string msg;
    public:
        str_exception (string s) : msg(s) {}
        virtual const char * what() const throw() { return msg.c_str(); }
        ~str_exception () throw() {}
};
