#include "poly.h"
#include <math.h>
#include <stdlib.h>
using namespace std;


/********** Overloaded operators **********/

Polynomial & Polynomial::operator+= (const Polynomial & summand)
{
    // adjust the degree and resize the vector
    d = d>summand.d ? d : summand.d;
    v.resize(d+1,0);

    // add the summand
    for (int i=0; i<= summand.d; i++)
        v[i] += summand.v[i];

    // shrink the degree and resize the vector again
    while (d>=0 && v[d]==0) d--; 
    v.resize(d+1,0);

    return *this;
}

Polynomial & Polynomial::operator-= (const Polynomial & summand)
{
    // adjust the degree and resize the vector
    d = d>summand.d ? d : summand.d;
    v.resize(d+1,0);

    // subtract the summand
    for (int i=0; i<= summand.d; i++)
        v[i] -= summand.v[i];

    // shrink the degree and resize the vector again
    while (d>=0 && v[d]==0) d--; 
    v.resize(d+1,0);

    return *this;
}

Polynomial & Polynomial::operator*= (const Polynomial & multiplicand)
{
    // if either are zero, just set this to zero and return
    if (d==-1 || multiplicand.d == -1) {
        v.resize(0);
        d=-1;
        return *this;
    }
    
    // do the multiplication
    vector<long long> w(d+multiplicand.d+1,0);
    for (int i=0; i<=d; i++)
        for (int j=0; j<=multiplicand.d; j++)
            w[i+j] += v[i]*multiplicand.v[j];

    // copy over the result
    v = w;
    d = d+multiplicand.d; 
    
    return *this;
}

Polynomial & Polynomial::operator*= (const long long s)
{
    for (int i=0; i<=d; i++)
        v[i]*=s;
    return *this;
}

Polynomial & Polynomial::operator+= (const long long a)
{
    (*this) += unit()*a;
    return *this;
}

Polynomial & Polynomial::operator-= (const long long a)
{
    (*this) -= unit()*a;
    return *this;
}

Polynomial operator+ (const Polynomial &p1, const Polynomial &p2) {
    Polynomial p(p1);
    p += p2;
    return p;
}

Polynomial operator- (const Polynomial &p1, const Polynomial &p2) {
    Polynomial p(p1);
    p -= p2;
    return p;
}

Polynomial operator* (const Polynomial &p1, const Polynomial &p2) {
    Polynomial p(p1);
    p *= p2;
    return p;
}

Polynomial operator+ (const Polynomial &p1, const long long a) {
    Polynomial p(p1);
    p += a;
    return p;
}

Polynomial operator- (const Polynomial &p1,  const long long a) {
    Polynomial p(p1);
    p -= a;
    return p;
}

Polynomial operator* (const Polynomial &p1, const long long s) {
    Polynomial p(p1);
    p *= s;
    return p;
}

Polynomial operator+ (const long long a, const Polynomial &p) {
    return p+a;
}
Polynomial operator- (const long long a, const Polynomial &p) {
    return (-1)*(p-a); 
}
Polynomial operator* (const long long s, const Polynomial &p) {
    return p*s;
}

ostream& operator<< (ostream &os, const Polynomial &p)
{
    if (p.d==-1) { 
        os << "0";
    }
    else { 
        for (int i=0; i<p.d; i++)
            if (p.v[i] != 0) os << p.v[i] << "*t^" << i << " + ";
        os << p.v[p.d] << "*t^" << p.d;
    }

    return os;
}

/********** Static factory functions **********/

// return a monomial of degree d
Polynomial Polynomial::monomial(unsigned d) {
    Polynomial p;
    p.v.resize(d+1,0);
    p.v[d]=1;
    p.d=d;
    return p;
}

/********** Special polynomial functions **********/

Polynomial & Polynomial::subs(const long long a)
{
    long long p=1;
    for (int i=0; i<=d; i++, p*=a)
        v[i]*=p;

    return *this;
}

Polynomial & Polynomial::shift(unsigned s)
{
    // if s==0 or *this is zero we don't need to do anything
    if (s != 0 && d != -1) {
        d+=s;
        v.resize(d+1);

        for (unsigned i=d; i >= s; i--)
            v[i]=v[i-s];

        while (s-- > 0) v[s]=0;
    }
            
    return *this;
}

Polynomial & Polynomial::reverse()
{
    vector<long long> w(d+1);
    for (int i=0; i<=d; i++)
        w[i] = v[d-i];
    v = w;
    return *this;
}

// assumes polynomial is monic
bool Polynomial::splits_helper(std::list<unsigned> &E, int m)
{
    if (d == 1) {
        E.push_back((-1)*v[0]);
        return 1;
    }
    else {
        // Use Horner's method to check all possible points between 0 and the
        // dth root of the absolute value of v[0].
        Polynomial p;
        p.v.resize(d+1,0);
        p.v[d]=0;
        int x=m-1;
        int N = floor(pow(abs(v[0]), 1/((double) d)));
        //int N = abs(v[0]);
        int r=-1;// the remainder of division
        // loop until we've found a root or we've all exhausted possible roots 
        do { 
            x++;
            // Here's where we use Horner's method
            for (int i=d-1; i>=0; i--) {
                p.v[i] = v[i+1] + x*p.v[i+1];
            }
            r = v[0] + x*p.v[0];
        } while( r!=0 && x<= N);
        if (r==0) {
            p.d = d-1;
            bool b = p.splits_helper(E,x);
            if (b) E.push_front(x);
            return b;
        }
        else {
            return 0;
        }
    }
}

// first make sure the polynomial is monic and the sign of v[0] is correct
bool Polynomial::splits(std::list<unsigned> &E)
{
    if (d<1) { 
        // in this case, we split if and only if we are one
        // since this is the ``empty product'' of linear factors
        return d==0 && v[0]==1;
    }
    else { 
        if (v[d] != 1) return 0; // not monic
        int s=v[0];
        for (int i=0; i<d; i++) s*=-1;
        if (s<0) return 0; // sign of constant term is not correct

        // no apparent problems, pass to helper
        return splits_helper(E,0);
    }
}
