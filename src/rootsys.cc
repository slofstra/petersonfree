#include "rootsys.h"
#include "matroid_exception.h"
#include "debug.h"
#include <math.h>
using namespace std;

/********** Constructors and initialization **********/
void Rootsystem::initialize_rootsystem ()
{
    initialize();

    if (!root_sys_initialized) {
        DEBUG_PRINT(3, "Initializing root system...");

        DEBUG_PRINT(3, "Constructing addition table");
        // First construct the addition table
        // loop through all pairs and try the addition
        for (int i=0; i<M.num_cols(); i++) {
            for (int j=0; j<M.num_cols(); j++) {
                addition_table(i,j)=-1; // default value
                if (i != j) {
                    // see if the output could be column k
                    // stop if column k is the output
                    for (int k=0; k<M.num_cols() && (addition_table(i,j) == -1); k++) {
                        // Loop through until we find a row where the sum of the pith entry 
                        // and the pjth entry is not equal to the pkth entry, or there are
                        // no more rows.
                        int p;
                        for (p=0; p<M.num_rows() && (M(p,i) + M(p,j) == M(p,k)); p++) ; // empty loop 

                        // if we got to the end, then column k is the sum of column i and column j
                        if (p==M.num_rows()) addition_table(i,j)=k;
                    }
                }
            }
        }


        DEBUG_PRINT(3, "Constructing list of convex sets...");
        // We use an unsigned long to enumerate all column sets, restricting us
        // to matroids of size less than sizeof(unsigned long)*8. (Above this size,
        // we are probably not going to be able to run this algorithm anyway.)
        if (M.num_cols() >= sizeof(unsigned long)*8) {
            throw str_exception("Matroid is to large to construct convex sets.");
        }

        // Now construct the convex sets: loop through all possible column
        // sets, and add the convex ones to the list.
        unsigned long L=pow(2, M.num_cols());
        for (unsigned long l=0; l<L; l++) {
            cbitset c(M.num_cols(), l); 
            if (is_convex(c)) convex.push_back(c);
        }

        DEBUG_PRINT(3, "Constructing list of coconvex sets...");
        // Now construct the coconvex sets as the complements of the convex
        // sets.
        coconvex = convex;
        for (list<cbitset>::iterator I = coconvex.begin(); I != coconvex.end(); I++) {
            I->flip(); 
        }

        root_sys_initialized=1;
    } 
}

/********** Rootsystem functions **********/

bool Rootsystem::is_convex(const cbitset &c) const
{
    for (int i=0; i<M.num_cols(); i++) {
        for (int j=0; j<M.num_cols(); j++) {
            if (c[i] && c[j] && addition_table(i,j) != -1 && !c[addition_table(i,j)]) {
                return 0;
            }
        }
    }
    return 1;
}

bool Rootsystem::is_coconvex(const cbitset &c) const
{
    cbitset d(c);
    return is_convex(d.flip());
}

bool Rootsystem::is_biconvex(const cbitset &c) const
{
    return is_convex(c) && is_coconvex(c);
}

// Peterson translation of set c, by root p
Rootsystem::cbitset Rootsystem::ptranslate(const cbitset &c, unsigned p) const
{ 
    cbitset d(c);
    return inv_ptranslate(d.flip(), p).flip();
}

Rootsystem::cbitset Rootsystem::inv_ptranslate(const cbitset &c, unsigned p) const
{
    if (p>= M.num_cols()) 
        throw new str_exception("Root index out of bounds in Peterson translation");

    cbitset oc = empty_cbitset();
    for (int i=0; i<M.num_cols(); i++) {
        if (c[i]) {
            int ne=i;
            for (int j=addition_table(i,p); j != -1; j=addition_table(j,p)) {
                if (!c[j]) ne = addition_table(ne,p);
            }
            oc[ne]=1;
        }
    }

    return oc;
}
