#include "matroid.h"
#include "matroid_exception.h"
#include "debug.h"
using namespace std;

/********** Matroid minor class **********/
bool Matroid::Minor::operator<(const Minor &other) const {
    return del<other.del || (del==other.del && res < other.res);
}

// Restrict or delete the hyperplane H. Note that we don't check that H is a
// valid hyperplane of the minor. In particular, it is assumed that H is
// disjoint from this->res.
Matroid::Minor Matroid::Minor::restrict (const cbitset & H) const {
    return Minor (del & (~H), res | H);
}
Matroid::Minor Matroid::Minor::delete_hyp (const cbitset & H) const {
    return Minor (del | H, res);
}
    

/********** Private helper functions **********/

void Matroid::gen_flats(const matrix & M, cbitset & flat, list<hyperplane> & hyperplanes, unsigned d)
{
    print_keepalive();
    for (list<hyperplane>::iterator I = hyperplanes.begin(); I != hyperplanes.end(); I++) {
        cbitset new_flat = flat | I->second;
        // see if we've created a new flat
        if (flat_hyperplanes.count(new_flat) == 0) {
            // if so, make a new matrix by copying the old matrix and clearing the new basis element 
            matrix new_matrix(M);
            new_matrix.column_clear(I->first);
            new_matrix.normalize();

            // now make the new list of hyperplanes by going through the old list and
            // seeing if we need to do any unions
            list<hyperplane> new_hyperplanes;
            for (list<hyperplane>::iterator J = hyperplanes.begin(); J != hyperplanes.end(); J++) {
                if (J != I) {
                    bool not_added = 1;
                    for (list<hyperplane>::iterator K = new_hyperplanes.begin(); K != new_hyperplanes.end() && not_added; K++) {
                        if (new_matrix.columns_parallel(J->first, K->first)) {
                            not_added = 0;
                            K->first = (K->first < J->first) ? K->first : J->first;
                            K->second |= J->second;
                        }
                    }
                    if (not_added) {
                        new_hyperplanes.push_back (*J);
                    }
                }
            }

            // add our new flat to the list of flats
            flat_hyperplanes.insert(pair<cbitset, Flat >(new_flat, Flat(d+1,new_hyperplanes)));

            // now recurse
            gen_flats(new_matrix, new_flat, new_hyperplanes,d+1);
        }
    }
}

// throws an exception if flat is not actually a flat
// initialize should already be called
Matroid::Flat & Matroid::lookup_flat(const cbitset & flat)
{
    map<cbitset, Flat >::iterator hyp = flat_hyperplanes.find(flat);
    if (hyp == flat_hyperplanes.end()) throw str_exception("flat is not a flat in hyperplanes");
    return hyp->second;
}

// Sort hyperplanes into buckets based on rank
void Matroid::sort_flats_by_rank()
{
    // the largest possible rank of a flat is the rank of the matroid + 1
    flats_by_rank.resize(rank()+1);
    for (map<cbitset,Flat>::iterator I = flat_hyperplanes.begin(); I != flat_hyperplanes.end(); I++) {
        flats_by_rank[I->second.rank].push_back(I->first);
    }
}

// Check that the triple (original, deletion, restriction) of exponents
// satisfies the conditions of the addition theorem.
// Note that the exponent lists must be sorted.
bool Matroid::check_exponents(const list<unsigned> &E, const list<unsigned> &Ed, const list<unsigned> &Er)
{
    list<unsigned>::const_iterator I, Id, Ir;
    I = E.begin(); Id = Ed.begin(); Ir = Er.begin();
    bool extra_found=0;
    while (I != E.end() || Id != Ed.end() || Ir != Er.end()) {
        if (I != E.end() && Id != Ed.end() && Ir != Er.end() 
            && *I == *Id && *Id == *Ir) {
            I++; 
            Id++; 
            Ir++;
        }
        else if (I != E.end() && Id != Ed.end() && *I == (*Id)+1 && (Ir == Er.end() || *Id != *Ir) && !extra_found) {
            extra_found=1;
            I++;
            Id++;
        }
        else {
            // If one of the above two situations doesn't hold, exponents don't
            // combine.
            return 0;
        }
    }
    return 1;
}

// This function looks up mi in the memoization table, and if not, returns
// a new guess. The ambient rank must be passed in as a parameter simply
// to avoid an extra lookup if called from a context where it is known.
Matroid::ExponentGuess &Matroid::guess_exponents(const Minor &mi, int amb_rank)
{
    DEBUG_PRINT(1, "Guessing exponents of " << mi << ", ambient rank is " << amb_rank);

    // try inserting a new key into the memoization table
    pair<map<Minor,ExponentGuess>::iterator, bool> GR 
        = memo_ind_free.insert(pair<Minor,ExponentGuess>(mi, ExponentGuess()));

    // for readability, set G to the actual guess by dereferencing the iterator
    // GR.first.
    ExponentGuess &G = GR.first->second;

    // If we added a new element, set up G to be the guess
    if (GR.second) {

        Polynomial p = poincare_poly(mi);
        G.eff_rank = p.degree(); 
        G.status = p.subs(-1).reverse().splits(G.exp) ? Uncertain : False;
        DEBUG_PRINT(1, "Adding " << G << " to the memoization table.");
        
        // if we have recorded exponents, must pad the exponent list with zeroes
        // so that we have ambient_rank exponents
        if (G.status != False) {
            int s = amb_rank - p.degree();
            while (s-- >0) G.exp.push_front(0);
        }
    }
    return G;
}

// Checks authoritively that the guess for mi is true. 
// Returns true if inductively free, and false otherwise.
// Also, after the call guess.status is either False or True.
// Note that 'guess' MUST BE A REFERENCE TO THE GUESS OBJECT IN THE 
// MEMOIZATION TABLE, or this won't work.
bool Matroid::check_guess(const Minor &mi, ExponentGuess &guess)
{
    DEBUG_PUSH;
    DEBUG_PRINT(1, "Checking guess for " << mi << ", current guess is " << guess);

    // If guess.status is False or True, then return immediately.
    if (guess.status != Uncertain) {

        DEBUG_POP;

        return guess.status == True;
    }

    // If effective rank <= 2, then always inductively free.
    if (guess.eff_rank <= 2) {
        DEBUG_POP;
        guess.status=True;
        return 1;
    }
    
    DEBUG_PRINT(1, "No quick answer, going through list of pivots");
    // go through the list of pivots
    Flat &F = lookup_flat(mi.res);
     
    for (list<hyperplane>::iterator I = F.hyperplanes.begin() ; I!=F.hyperplanes.end(); I++) {
        // see if the current hyperplane is a valid pivot of the minor
        if (!I->second.is_subset_of(mi.del)) {
            DEBUG_PRINT(1, "Found pivot " << I->second << ", represented by " << I->first);            

            // construct the minors
            Minor mi_d = mi.delete_hyp(I->second); 
            Minor mi_r = mi.restrict(I->second);
    
            // guess the exponents
            ExponentGuess &guess_d = guess_exponents(mi_d, M.num_rows() - F.rank); //ambient rank same as mi
            ExponentGuess &guess_r = guess_exponents(mi_r, M.num_rows() - F.rank-1); //ambient rank decreases by 1

            DEBUG_PRINT(1, "For deletion " << mi_d << ", guess is " << guess_d);
            DEBUG_PRINT(1, "For restriction " << mi_r << ", guess is " << guess_r);

            // Check the following in sequence: (1) that neither mi_d or mi_r are known to be
            // not inductively free, (2) that the exponents combine correctly, and (3) that
            // check_guess returns true (verifying that the minors are indeed inductively free).
            // Relies on short-circuit evaluation. 
            if (guess_d.status != False && guess_r.status != False               // (1)
                && Matroid::check_exponents(guess.exp, guess_d.exp, guess_r.exp) // (2)
                && check_guess(mi_d, guess_d) && check_guess(mi_r, guess_r))     // (3)
            {
                DEBUG_PRINT(1, "Guess is verified, " << mi << " is inductively free.");
                DEBUG_POP;

                // guess was correct, we are inductively free
                guess.status = True; 
                return 1;
            }
        }
    }

    DEBUG_PRINT(1, "Guess is incorrect, " << mi << " is not inductively free.");
    DEBUG_POP;

    // guess was not correct, we are not inductively free
    guess.status = False;
    // for now, let's keep the exponent list around
    // guess.exp.erase(guess.exp.begin(), guess.exp.end());
    return 0;
}

/********** Matroid functions **********/
void Matroid::initialize() 
{
    if (!initialized) {
        // generate the list of flats by adding the empty flat, and then
        // calling gen_flats
        cbitset c = empty_cbitset();
        list<hyperplane> L;
        for (unsigned i=0; i<M.num_cols(); i++) {
            cbitset h = empty_cbitset();
            h[i] = 1; 
            L.push_back(hyperplane(i, h));
        }
        flat_hyperplanes.insert(pair<cbitset, Flat>(c,Flat(0,L)));
        gen_flats(M, c, L,0);

        sort_flats_by_rank();

        // done initialization
        initialized = 1;
        DEBUG_PRINT(3, flat_hyperplanes.size() << " flats.");
    }
}

unsigned Matroid::flat_rank (const cbitset &f) {
    return lookup_flat(f).rank;
}
unsigned Matroid::ambient_rank(const Minor &mi) {
    return M.num_rows() - flat_rank(mi.res);
}
unsigned Matroid::effective_rank(const Minor &mi) {
    return poincare_poly(mi).degree();
} 

// return a list of rank k flats
const list<Matroid::cbitset> & Matroid::flats (int k)
{
    return flats_by_rank[k];
}

// Computes the poincare polynomial for the essential arrangement corresponding to
// the minor mi.
Polynomial Matroid::poincare_poly (const Minor & mi)
{
    map<Minor,Polynomial >::iterator memo = memo_poincare_poly.find(mi);
    if (memo != memo_poincare_poly.end()) { 
        return memo->second;
    }
    print_keepalive();

    // get the list of hyperplanes, exception thrown in hyperplanes if mi.res
    // is not a flat
    list<hyperplane> & HL = lookup_flat(mi.res).hyperplanes;

    // a pivot is a hyperplane that is not contained inside the list of deleted hyperplanes
    list<hyperplane>::iterator I = HL.begin();
    for ( ; I != HL.end() && I->second.is_subset_of(mi.del); I++) ; // empty loop

    Polynomial p;
    if (I == HL.end()) {
        p = Polynomial::unit();
    }
    else {
        // otherwise we have a valid pivot, and can delete and restrict this pivot
        p = poincare_poly(mi.delete_hyp(I->second)) + poincare_poly(mi.restrict(I->second)).shift();
    }
    
    //memoize the answer
    memo_poincare_poly.insert(pair<Minor, Polynomial >(mi, p));
    return p;
}
 
Polynomial Matroid::char_poly (const Minor & mi)
{
    Polynomial p = poincare_poly(mi);
    int s = ambient_rank(mi) - p.degree();
    return p.subs(-1).reverse().shift(s);
}

bool Matroid::ind_free (const Minor &mi, list<unsigned> &E)
{
    // guess at the exponents
    ExponentGuess &G = guess_exponents(mi, ambient_rank(mi));

    if (check_guess(mi, G)) { 
        E = list<unsigned>(G.exp);
        return 1;
    }
    else { 
        return 0; 
    }
}

void Matroid::print_certificate(const Minor &mi, ostream &os)
{
    // start with the effective rank
    int ef = effective_rank(mi);

    DEBUG_PUSH;
    DEBUG_PRINT(2, "Starting " << mi);
    DEBUG_PRINT(2, "Effective rank of " << mi << " is " << ef);

    // if the effective rank is <= 2, print an empty list.
    if (ef <= 2) {
        DEBUG_PRINT(2, "Effective rank is " << ef << ", bottom of tree.");
        os << "[]";
    }
    // otherwise we need to recurse
    else { 
        // Get the list of hyperplanes
        Flat &F = lookup_flat(mi.res);
        list<hyperplane> &L = F.hyperplanes;

        bool pivot_found=0; // whether or not we've found a pivot yet
        int amb_rank = ambient_rank(mi);

        // Record our current exponents
        ExponentGuess &G = guess_exponents(mi, amb_rank);

        DEBUG_PRINT(2, "Current guess for " << mi << " is " << G);

        // Make sure that we are actually following a known path
        if (G.status != True) {
            throw str_exception("Minor is not known to be inductively free in print_certificate");
        }

        // iterate until we find a good pivot
        for (list<hyperplane>::iterator I = L.begin(); I != L.end() && !pivot_found; I++) {
            // make sure *I is a valid pivot
            if (!I->second.is_subset_of(mi.del)) {
                DEBUG_PRINT(2, "Found a valid pivot: " << I->second);

                // look up our recorded guess information
                ExponentGuess &Gd = guess_exponents(mi.delete_hyp(I->second), amb_rank);
                ExponentGuess &Gr = guess_exponents(mi.restrict(I->second), amb_rank);

                DEBUG_PRINT(2, "Guess for deletion " << mi.delete_hyp(I->second) << " is " << Gd);
                DEBUG_PRINT(2, "Guess for restriction " << mi.restrict(I->second) << " is " << Gr);
            
                // Now see if the pivot actually works
                if ( Gd.status == True && Gr.status == True && check_exponents(G.exp, Gd.exp, Gr.exp)) {


                    pivot_found=1;
                    int i;
                    // empty loop, find first index i such that I->second[i]==1 && mi.del[i]==0
                    for (i=0; i<I->second.size() && (I->second[i] == 0 || mi.del[i]==1) ; i++) ;

                    if (i==I->second.size()) 
                        throw new str_exception("Could not find representative of hyperplane in print_certificate");
                    DEBUG_PRINT(2, "Pivot works! Representative is " << i);

                    os << "[" << i << ","; 
                    print_certificate(mi.delete_hyp(I->second), os);
                    os << ",";
                    print_certificate(mi.restrict(I->second), os);
                    os << "]";
                }
            }
        }

        if (!pivot_found)
            throw str_exception("Couldn't find pivot in print_certificate");
    }

    DEBUG_POP;
}

// Computes the freeness of the arrangement of the given minor.  If the char.
// poly splits, but the minor is not inductively free, then we can't determine
// freeness, and we return Uncertain.
Matroid::FuzzyBool Matroid::is_free (const Minor &mi, list<unsigned> &E)
{
    list<unsigned> Ep;
    DEBUG_PRINT(3, "Checking " << mi);
    DEBUG_PUSH;
    // If characteristic polynomial doesn't split, definitely non-free
    if (!char_poly(mi).splits(Ep)) {
        DEBUG_PRINT(3, "Char. poly doesn't split");
        DEBUG_POP;
        return False;
    }
    // If we are inductively free, then we are free. 
    // Put exponents into E.
    else if (ind_free(mi, E)) {
        DEBUG_PRINT(3, "Ind. free");
        DEBUG_POP;
        return True;
    }
    // In this case, the characteristic polynomial split, but we weren't
    // inductively free. 
    else {
        DEBUG_PRINT(3, "Uncertain");
        DEBUG_POP;
        return Uncertain;
    }
}

Matroid::FuzzyBool Matroid::is_free (const Minor &mi)
{
    list<unsigned> E;
    return is_free(mi, E);
}

// The argument is a column set describing a submatroid.
// Return:
// - True if argument has full rank, is non-free, and all corank one flats are
//   inductively free.
// - False if argument has less than full rank, is inductively free, or has
//   a corank one flat which is not inductively free.
// - Uncertain if we can't determine freeness of argument, or the freeness of
//   one of its corank one flats.
Matroid::FuzzyBool Matroid::is_minimal_non_free(const cbitset &submat)
{
    // Calculate the complement, and the minor corresponding to the submatroid
    cbitset complement(submat);
    complement.flip();
    Minor mi(complement, empty_cbitset());

    // Determine the effective rank, and make sure the submatroid has full rank.
    int r = effective_rank(mi);
    if (r < rank()) return False;

    DEBUG_PRINT(3, submat << " has full rank.");

    // See if we are free
    list<unsigned> E;
    FuzzyBool isfree = is_free(mi);
    if (isfree==True) {
        // Get a certain answer from is_free if and only if we are inductively free,
        // in which case, definitely not non-free.
        DEBUG_PRINT(3, submat << " is inductively free.");
        return False; 
    }
    else {
        // Otherwise we go through the corank one flats and look for one that is non-free.
        DEBUG_PRINT(3, submat << " is not inductively free, going through corank one flats");
        DEBUG_PUSH;
            
        // Go through all flats of rank k, for 3 <= k <= r-1.
        FuzzyBool flats_free = True; // whether or not all the flats are free
        for (int k = 3; k < r && flats_free==True; k++) { 
            const list<cbitset> &L = flats(k);
            for (list<cbitset>::const_iterator I = L.begin(); I != L.end() && flats_free == True; I++) {
                // calculate the intersection of the flat with the submatroid
                cbitset fl = *I & submat;

                DEBUG_PRINT(3, "Checking flat " << fl);

                // Now see if this flat is free
                flats_free = is_free(Minor(fl.flip(), empty_cbitset())); // fl is now the complement
            }
        }

        DEBUG_PRINT(3, "Last flat was " << flats_free);
        DEBUG_POP;

        // To be minimally non-free, we need to be provably non-free,
        // and all flats need to be free.
        return flats_free && !isfree;
    }
}

// The argument is a column set describing a submatroid.
// Return:
// - True if the argument is inductively free. 
// - False if the characteristic polynomial does not split, or there is
//   a corank one flat whose characteristic polynomial does not split.
// - Uncertain if the submatroid is not inductively free, but the characteristic
//   polynomial of the matroid and all corank one flats does split.
// This function should be used in preference to is_free (for
// submatroids) if it is important to eliminate as many Uncertain
// answers as possible. 
Matroid::FuzzyBool Matroid::is_free_submatroid(const cbitset &submat)
{
    // Calculate the complement, and the minor corresponding to the submatroid
    cbitset complement(submat);
    complement.flip();
    Minor mi(complement, empty_cbitset());

    // See if we are free
    FuzzyBool isfree = is_free(mi);

    // If we get a certain answer, then we can return that
    if (isfree!=Uncertain) {
        return isfree;
    }
    else {
        // Otherwise we go through all flats and look for one that is provably non-free.
        int r = rank();
        for (int k = 3; k<r-1; k++) { 

            const list<cbitset> &L = flats(k);
            // Look for a flat which is provably not free
            for (list<cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
                // calculate the intersection of the flat with the submatroid
                cbitset fl = *I & submat;

                // if this flat is non-free, then exit---this submatroid is definitely not free
                if (is_free(Minor(fl.flip(), empty_cbitset())) == False) { // fl is now the complement
                    return False;
                }
            }
        }

        // If we got this far, then we must still be uncertain
        return Uncertain;
    }
}

/********** Operations **********/

ostream& operator<< (ostream &os, const Matroid::cbitset &c)
{
    os << "ColSet(";
    bool first=1;
    for (int i=0; i<c.size(); i++) {
        if (c[i]) { 
            if (first) { 
                os << (i+1);
                first =0;
            }
            else {
                os << ", " << (i+1);
            }
        }
    }
    os << ")";

    return os;
}

ostream& operator<< (ostream &os, const Matroid::Minor &mi)
{
    os << "Minor(" << mi.del << ", " << mi.res << ")";
    
    return os;
}

ostream& operator<< (ostream &os, const Matroid::ExponentGuess &EG)
{
    os << "Guess(";
    if (EG.status == Matroid::False) {
        os << "not IF)";
    }
    else {
        bool first=1;
        for (list<unsigned>::const_iterator I = EG.exp.begin(); I != EG.exp.end(); I++) {
            if (first) { 
                os << *I;
                first =0;
            }
            else {
                os << ", " << *I;
            }
        }
        if (first) {
            os << "eff. rank = " << EG.eff_rank;
            first=0;
        }
        else {
            os << ", eff. rank = " << EG.eff_rank;
        }
        if (EG.status == Matroid::Uncertain) {
            os << " (uncertain))";
        }
        else {
            os << " (certain))";
        }
    }
    
    return os;
}

ostream& operator<< (ostream &os, const Matroid::FuzzyBool &FB)
{
    os << "Fuzzy(";
    if (FB==Matroid::True) os << "True";
    else if (FB==Matroid::False) os << "False";
    else os << "Uncertain";
    os << ")";
    return os;
}

// And in fuzzy logic
Matroid::FuzzyBool operator&& (const Matroid::FuzzyBool &b1, const Matroid::FuzzyBool &b2)
{
    if (b1==Matroid::False || b2==Matroid::False) {
        return Matroid::False;
    }
    else if (b1 == Matroid::True && b2==Matroid::True) {
        return Matroid::True;
    }
    else { 
        return Matroid::Uncertain;
    }
}

Matroid::FuzzyBool operator! (const Matroid::FuzzyBool &b)
{
    switch(b) {
        case Matroid::True:
            return Matroid::False;
            break;
        
        case Matroid::False:
            return Matroid::True;
            break;

        case Matroid::Uncertain:
            return Matroid::Uncertain;
            break;
    }
}
