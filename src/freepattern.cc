#include "rootsys.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void show_help() 
{
    cout << "Usage: freepattern command [params]" << endl << endl;
    cout << "where command is one of: convex, coconvex, biconvex, min, comin, bimin" << endl << endl;
    cout << "1. freepattern convex" << endl;
    cout << "2. freepattern coconvex" << endl;
    cout << "3. freepattern biconvex" << endl;
    cout << "Read in a matrix from standard input, and treat the columns as specifying a" << endl;
    cout << "rootsystem in some basis. Prints the convex (resp. coconvex, biconvex) sets of" << endl;
    cout << "this rootsystem." << endl << endl;
    cout << "4. freepattern min" << endl;
    cout << "5. freepattern comin" << endl;
    cout << "6. freepattern bimin" << endl;
    cout << "Reads in a root system from standard input, and outputs the minimal non-free" << endl;
    cout << "convex (resp. coconvex, biconvex) sets of top rank." << endl << endl;
    cout << "7. freepattern nonfree" << endl;
    cout << "Reads in a root system from standard input, and outputs all non-free" << endl;
    cout << "biconvex sets." << endl << endl; 
}

void convex_sets() {
    matrix m = matrix::read();
    matrix::print(m);

    Rootsystem R(m);
    cout << "Initializing...";
    cout.flush();
    R.initialize_rootsystem();
    cout << "done" << endl;

    const list<Matroid::cbitset> L = R.convex_sets();
    cout << L.size() << " convex sets" << endl << endl;
    for (list<Matroid::cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
        cout << *I << endl;
    }
}

void coconvex_sets() { 
    matrix m = matrix::read();
    matrix::print(m);

    Rootsystem R(m);
    cout << "Initializing...";
    cout.flush();
    R.initialize_rootsystem();
    cout << "done" << endl;

    const list<Matroid::cbitset> L = R.coconvex_sets();
    cout << L.size() << " coconvex sets" << endl << endl;
    for (list<Matroid::cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
        cout << *I << endl;
    }
}

void biconvex_sets() { 
    matrix m = matrix::read();
    matrix::print(m);

    Rootsystem R(m);
    cout << "Initializing...";
    cout.flush();
    R.initialize_rootsystem();
    cout << "done" << endl;
    
    // keep track of how many biconvex sets we find
    int count=0;

    const list<Matroid::cbitset> L = R.coconvex_sets();
    for (list<Matroid::cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
        if (R.is_convex(*I)) {
            cout << *I << endl;
            count++;
        }
    }
    cout << count << " biconvex sets found" << endl << endl;
}

void minimal_convex_sets() { 
    matrix m = matrix::read();
    matrix::print(m);

    Rootsystem R(m);
    cout << "Initializing...";
    cout.flush();
    R.initialize_rootsystem();
    cout << "done" << endl;

    cout << "Printing minimal non-free convex sets..." << endl;

    // Keep track of the minimal and ambiguous elements
    int count=0, count_amb=0;

    // Go through the coconvex set
    const list<Matroid::cbitset> L = R.convex_sets();
    for (list<Matroid::cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
        switch(R.is_minimal_non_free(*I)) {
            case Matroid::True:
                cout << *I << " (minimal non-free)" << endl;
                count++;
                break;
            
            case Matroid::Uncertain:
                cout << *I << " (AMBIGUOUS!!!)" << endl;
                count_amb++;
                break;
    
            case Matroid::False:
                break;
        }
    }
    cout << endl << count << " minimal non-free convex sets found" << endl;
    if (count_amb==0) {
        cout << "Freeness could be determined for all convex sets." << endl;
    }
    else {
        cout << count_amb << " convex sets where freeness could not be determined!!!" << endl;
    }
}

void minimal_coconvex_sets() { 
    matrix m = matrix::read();
    matrix::print(m);

    Rootsystem R(m);
    cout << "Initializing...";
    cout.flush();
    R.initialize_rootsystem();
    cout << "done" << endl;

    cout << "Printing minimal non-free coconvex sets..." << endl;

    // Keep track of the minimal and ambiguous elements
    int count=0, count_amb=0;

    // Go through the coconvex set
    const list<Matroid::cbitset> L = R.coconvex_sets();
    for (list<Matroid::cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
        switch(R.is_minimal_non_free(*I)) {
            case Matroid::True:
                cout << *I << " (minimal non-free)" << endl;
                count++;
                break;
            
            case Matroid::Uncertain:
                cout << *I << " (AMBIGUOUS!!!)" << endl;
                count_amb++;
                break;
    
            case Matroid::False:
                break;
        }
    }
    cout << endl << count << " minimal non-free coconvex sets found" << endl;
    if (count_amb==0) {
        cout << "Freeness could be determined for all coconvex sets." << endl;
    }
    else {
        cout << count_amb << " coconvex sets where freeness could not be determined!!!" << endl;
    }
}

void minimal_biconvex_sets() { 
    matrix m = matrix::read();
    matrix::print(m);

    Rootsystem R(m);
    cout << "Initializing...";
    cout.flush();
    R.initialize_rootsystem();
    cout << "done" << endl;

    cout << "Printing minimal non-free biconvex sets..." << endl;

    // Keep track of the minimal and ambiguous elements
    int count=0, count_amb=0;

    // Go through the coconvex set
    const list<Matroid::cbitset> L = R.coconvex_sets();
    for (list<Matroid::cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
        if (R.is_convex(*I)) {
            switch(R.is_minimal_non_free(*I)) {
                case Matroid::True:
                    cout << *I << " (minimal non-free)" << endl;
                    count++;
                    break;
                
                case Matroid::Uncertain:
                    cout << *I << " (AMBIGUOUS!!!)" << endl;
                    count_amb++;
                    break;
    
                case Matroid::False:
                    break;
            }
        }
    }
    cout << endl << count << " minimal non-free biconvex sets found" << endl;
    if (count_amb==0) {
        cout << "Freeness could be determined for all biconvex sets." << endl;
    }
    else {
        cout << count_amb << " biconvex sets where freeness could not be determined!!!" << endl;
    }
}

void nonfree_biconvex_sets() {
    matrix m = matrix::read();
    matrix::print(m);

    Rootsystem R(m);
    cout << "Initializing...";
    cout.flush();
    R.initialize_rootsystem();
    cout << "done" << endl;

    cout << "Printing all non-free biconvex sets..." << endl;

    // Keep track of the nonfree and ambiguous elements
    int count=0, count_amb=0;

    // Go through the coconvex set
    const list<Matroid::cbitset> L = R.coconvex_sets();
    for (list<Matroid::cbitset>::const_iterator I = L.begin(); I != L.end(); I++) {
        if (R.is_convex(*I)) {
            Matroid::cbitset complement(*I);
            complement.flip();
            switch(R.is_free(Matroid::Minor(complement, R.empty_cbitset()))) {
                case Matroid::False:
                    cout << *I << " (non-free)" << endl;
                    count++;
                    break;
                
                case Matroid::Uncertain:
                    cout << *I << " (AMBIGUOUS!!!)" << endl;
                    count_amb++;
                    break;
    
                case Matroid::True:
                    break;
            }
        }
    }
    cout << endl << count << " non-free biconvex sets found" << endl;
    if (count_amb==0) {
        cout << "Freeness could be determined for all biconvex sets." << endl;
    }
    else {
        cout << count_amb << " biconvex sets where freeness could not be determined!!!" << endl;
    }
}

int main (int argc, char *argv[])
{
    int mode=0; // mode of operation: -1 == error in parameters, 0 = no mode specified, 
    char *cert_file;
    
    // must be at least one parameter
    if (argc<2) {
        mode=-1;
    }
    else {
        string command = argv[1];
        if (command == "convex") {
            mode=4;
        }
        else if (command == "coconvex") {
            mode=5;
        }
        else if (command == "biconvex") {
            mode=6;
        }
        else if (command == "min") {
            mode=7;
        }
        else if (command == "comin") {
            mode=8;
        }
        else if (command == "bimin") {
            mode=9;
        }
        else if (command == "nonfree") {
            mode=10; 
        }
    }

    if (mode==-1) {
        show_help();
        return 1;
    }
    else if (mode==0) {
        cout << "No mode specified." << endl;
        show_help();
    }
    else if (mode==4) {
        convex_sets();
    }
    else if (mode==5) {
        coconvex_sets();
    }
    else if (mode==6) {
        biconvex_sets();
    }
    else if (mode==7) {
        minimal_convex_sets();
    }
    else if (mode==8) {
        minimal_coconvex_sets();
    }
    else if (mode==9) {
        minimal_biconvex_sets();
    }
    else if (mode==10) {
        nonfree_biconvex_sets();
    }

    return 0;
}

