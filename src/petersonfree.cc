#include "ptgraph.h"
#include "io.h"
#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>
using namespace std;

void show_help() 
{
    cout << "Usage: petersonfree [n]" << endl;
    cout << "Reads in a rootsystem from standard input, computes the Peterson graph," << endl;
    cout << "and then determines n-Peterson freeness of each coconvex set. If n" << endl;
    cout << "is omitted or is negative then freeness is not tested." << endl << endl; 

    cout << "Usage: petersonfree help|-h" << endl;
    cout << "Shows this message." << endl << endl;

    cout << "Defn 1. The Peterson graph of a rootsystem has the coconvex sets as" << endl;
    cout << "nodes, and a directed edge between two coconvex sets C1 and C2 if there" << endl;
    cout << "is root in C1 such that translating C1 by that root gives C2." << endl << endl;

    cout << "Defn 2. A coconvex set is n-Peterson free if the k-step Peterson translates" << endl;
    cout << "give free hyperplane arrangements, for 0 <= k <= n." << endl << endl;
}


// Read in matrix from standard in, print out Peterson graph to standard out
void ptgraph(int n) 
{
    matrix m = matrix::read();
    matrix::print(m);

    PTGraph ptg(m);
    cout << "Initializing...";
    cout.flush();
    ptg.initialize_graph(n);
    cout << "done" << endl << endl;
    cout.flush();

    PTGraph::print(ptg);
}

int main (int argc, char *argv[])
{
    int mode=0; // mode of operation: -1 == error in parameters, 0 = show help, 
                // 1 = standard operation

    int n;
    
    if (argc<2) {
        mode=1;
        n = -1;
    }
    else if (argc==2) { 
        string command = argv[1];
        if (command == "help" || command == "-h") { 
            mode = 0;
        }
        else {
            try {
                n = boost::lexical_cast<int>(command);
                mode = 1;
            }
            catch (boost::bad_lexical_cast const&) {
                mode = -1;
            }
        }
    }
    else { 
        mode = -1;
    }

    if (mode==-1) {
        cout << "Error in parameters." << endl;
        show_help();
        return 1;
    }
    else if (mode==0) {
        show_help();
    }
    else if (mode==1) {
        ptgraph(n); 
    }

    return 0;
}
