#include <iostream>
#include "ptgraph.h"
using namespace std;

void PTGraph::initialize_graph(int k)
{
    if (!initialized) {
        R.initialize_rootsystem();

        // Get the list of coconvex sets
        const list<Rootsystem::cbitset> & coconvex_sets = R.coconvex_sets();

        // set up the map from cbitsets to nodes
        for (list<Rootsystem::cbitset>::const_iterator it = coconvex_sets.begin(); 
                                                        it != coconvex_sets.end(); it++) {
            nodes[*it]; // will create the element
        }

        // Now iterate through the map and set up the indices and
        // the edges.
        { 
        map<Rootsystem::cbitset,node>::iterator it = nodes.begin(); // has local scope
        for (int i = 0; it != nodes.end(); it++, i++) {

            // set the index to i
            it->second.i = i;

            // calculate freeness if k>=0
            if (k >= 0) {
                it->second.k=0;
                it->second.is_free = R.is_free_submatroid(it->first);
            }

            // now compute all the edges
            for (unsigned i=0; i < it->first.size(); i++) {
                Rootsystem::cbitset cn = R.ptranslate(it->first, i);

                // set edge label while adding cn to the list
                if (cn != it->first) it->second.edges[cn]=i; 

            }
        }
        } // end local scope of iterator 'it'

        // Calculate j-freeness for j>=1.
        // On the jth interation, we check whether or not j-free coconvex sets
        // are (j+1)-free. 
        for (int j=0; j<k; j++) {
            for (map<Rootsystem::cbitset,node>::iterator it = nodes.begin(); 
                                                            it != nodes.end(); it++) {
                // first make sure that the current node is j-free.
                // if it's not then we don't do anything.
                if (it->second.is_pt_free(j)) {
                    // now check that all adjacent nodes are j-free by iterating through all edges
                    map<Rootsystem::cbitset, unsigned>::iterator eit = it->second.edges.begin(); 
                    while (eit != it->second.edges.end() && nodes[eit->first].is_pt_free(j)) eit++;

                    if (eit != it->second.edges.end()) {
                        // if we didn't reach the end, then an adjacent node is not j-free, with ending
                        // condition False or Ambiguous. Hence current not is not (j+1)-free, and
                        // ending condition is the same as the adjacent node. 
                        it->second.is_free = nodes[eit->first].is_free;
                    }

                    // since we checked (j+1)-freeness, we need to update k.
                    it->second.k=j+1;
                }
            }
        }

        // Don't initialize again
        initialized=1;
    }
}

int PTGraph::index(const Rootsystem::cbitset &c) const
{
    map<Rootsystem::cbitset,PTGraph::node>::const_iterator res = nodes.find(c);
    if (res==nodes.end()) {
        return -1;
    }
    else {
        return res->second.i;
    }
}

void PTGraph::print (const PTGraph &ptg)
{
    cout << "Peterson graph with " << ptg.size() << " nodes " << endl << endl;

    for (map<Rootsystem::cbitset, node>::const_iterator it = ptg.nodes.begin(); 
                                                it != ptg.nodes.end(); it++) { 
        cout << it->second.i << ". " << it->first << ", Edges = ";

        // print out the edge set
        map<Rootsystem::cbitset, unsigned>::const_iterator eit = it->second.edges.begin();
        if (eit != it->second.edges.end()) { 
            try { cout << ptg.nodes.at(eit->first).i << " (" << (eit->second+1) << ")"; }
            catch (const std::out_of_range&) {
                cout << "ERROR: " << eit->first << " is not a coconvex set!!!" << endl;
            }
            eit++;
            while (eit != it->second.edges.end()) { 
                try { cout << ", " << ptg.nodes.at(eit->first).i << " (" << (eit->second+1) << ")"; }
                catch (const std::out_of_range&) { 
                    cout << "ERROR: " << eit->first << " is not a coconvex set!!!" << endl;
                }
                eit++;
            }
        }
        else {
            cout << "NULL";
        }

        // output the freeness properties
        cout << ", ";
        switch(it->second.is_free) { 
            case Rootsystem::True:
                if (it->second.k==-1) {
                    cout << "Freeness not tested";
                }
                else {
                    cout << it->second.k << "-free";
                }
                break;

            case Rootsystem::Uncertain:
                cout << it->second.k << "-freeness is AMBIGUOUS!!!";
                break;

            case Rootsystem::False:
                if (it->second.k > 0) {
                    cout << (it->second.k-1) << "-free but not " << it->second.k << "-free";
                }
                else {
                    cout << "not 0-free";
                }
                break;
        }
        cout << endl;
    }
}
