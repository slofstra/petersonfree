#include <utility>
#include <boost/dynamic_bitset.hpp>
#include <list>
#include <map>
#include <iostream>
#include <vector>
#include "matrix.h"
#include "poly.h"

class Matroid {
    public: 
        /********** Public types **********/
        // a subset of the columns
        typedef boost::dynamic_bitset<> cbitset;

        // a hyperplane consists of an int, containing the minimal
        // representative of the hyperplane class, and a cbitset containing
        // all the representatives.
        typedef std::pair<unsigned, cbitset> hyperplane;

        // Records the data for each flat: the list of hyperplanes, and the rank.
        class Flat {
            public: 
                unsigned rank;
                std::list<hyperplane> hyperplanes;
                Flat(const unsigned r, const std::list<hyperplane> & L) : rank(r), hyperplanes(L) {}
        };

        // Minors (M \ del) | res are represented by the following class.  Note
        // that res should be a closed flat, and del and res should be disjoint
        // in most applications. While these are not checked, behaviour if
        // these conditions are not met is undefined.
        class Minor {
            public: 
                const cbitset del, res;

                Minor(const cbitset & d, const cbitset & r) : del(d), res(r) {} 
                bool operator<(const Minor &) const;
                Minor restrict (const cbitset &) const;
                Minor delete_hyp (const cbitset &) const;
        };

        // An uncertain truth value.
        enum FuzzyBool { False=0, Uncertain=1, True=2 };

        // A guess as to whether or not the minor is inductively free.
        // Contains the guess as to the exponents, as well as a variable 'status'
        // which is False if the minor is not inductively free, Uncertain if the
        // characteristic polynomial splits but the minor is not known to be
        // inductively free, and True if the minor is known to be inductively
        // free. Finally, 'eff_rank' contains the effective rank, which is the
        // the degree of the Poincare polynomial.
        class ExponentGuess {
            public:
                FuzzyBool status;
                int eff_rank;
                std::list<unsigned> exp;
        };

    protected:
        /********** Protected data members **********/
        const matrix M; // underlying matrix

    private:
        /********** Private types **********/

        /********** Private data members **********/
        // true if all pre-computed functions are initialized, false otherwise
        bool initialized;

        std::map<cbitset, Flat > flat_hyperplanes; // the hyperplanes for each flat
        std::vector<std::list<cbitset> > flats_by_rank; // the flats sorted by rank

        //memoization map for poincare poly
        std::map<Minor, Polynomial > memo_poincare_poly; 

        // memoization map for ind free
        // targets should always be sorted
        std::map<Minor, ExponentGuess> memo_ind_free;

        /********** Private helper functions **********/

        // intersection lattice stuff
        void gen_flats(const matrix &, cbitset &, std::list<hyperplane> &, unsigned);
        Flat & lookup_flat(const cbitset &);
        void sort_flats_by_rank();

        // inductive freeness
        static bool check_exponents(const std::list<unsigned> &, const std::list<unsigned> &, const std::list<unsigned> &);
        ExponentGuess & guess_exponents(const Minor &, int);
        bool check_guess(const Minor &, ExponentGuess &);

    public:
        /********** Constructors **********/
        // Create a matroid from a matrix
        Matroid (const matrix & init) : M(init), initialized(0) {}

        /********** Operators **********/
        // For equality testing, two matroids are equal if their underyling matrices
        // are equal.
        bool operator== (const Matroid & other) const { return M == other.M; }

        /********** Matroid functions **********/
        // create types
        cbitset empty_cbitset () const { return cbitset(M.num_cols(), 0); }
        Minor empty_minor () const { return Minor(empty_cbitset(), empty_cbitset()); }

        // Initialize the intersection lattice.
        // flat_rank computes the rank of a flat, ambient_rank computes the
        // ambient rank of a minor, and effective_rank computes the effective
        // rank of a minor. rank computes the rank of the matroid.
        void initialize(); 
        unsigned flat_rank(const cbitset &);
        unsigned ambient_rank(const Minor &);
        unsigned effective_rank(const Minor &); 
        unsigned rank() { return effective_rank(empty_minor()); }
    
        // Return a list of rank k flats. Each flat is specified as a cbitset.
        const std::list<cbitset> & flats (int);

        // Computes the Tutte polynomial, the characteristic polynomial, and the
        // positive roots of the characteristic polynomial for any minor of the
        // matroid.  A minor (M \ del) | res is specified by the two sets del
        // and res.  Note that restrictions must be done after deletions.  res
        // must be closed, and res and del must be disjoint. 
        Polynomial poincare_poly (const Minor &);
        Polynomial poincare_poly () { return poincare_poly(empty_minor()); }
        Polynomial char_poly (const Minor &);
        Polynomial char_poly () { return char_poly(empty_minor()); }

        // Computes inductive freeness of the non-essential arrangement of the given minor. 
        // If inductively free, puts the exponents into the supplied list reference.
        // If not inductively free, the contents of E are unchanged.
        bool ind_free (const Minor &, std::list<unsigned> &);
        bool ind_free (std::list<unsigned> &E) { return ind_free(empty_minor(), E); }

        // Print a certificate of inductive freeness for the given minor to the
        // given ostream&. A certificate has the following format:
        // C is either an empty list [] if the minor is trivial, or
        // C = [n, C1, C2], where n is the pivot, C1 is a certificate of
        // inductive freeness for M \ n, and C2 is a certificate of inductive
        // freeness for M / n.
        // Note: if ind_free hasn't been called on mi already, then an exception
        // might be thrown.
        void print_certificate(const Minor &, std::ostream &);

        // Computes the freeness of the arrangement of the given minor.
        // If the char. poly splits, but the minor is not inductively free, then we
        // can't determine freeness, and we return Uncertain.
        FuzzyBool is_free (const Minor &, std::list<unsigned> &);
        FuzzyBool is_free (const Minor &);
    
        // The argument is a column set describing a submatroid.
        // Return:
        // - True if argument has full rank, is non-free, and all corank one
        //   flats are inductively free.
        // - False if argument has less than full rank, is inductively free, or
        //   has a corank one flat which is not inductively free.
        // - Uncertain if we can't determine freeness of argument, or the
        //   freeness of one of its corank one flats.
        FuzzyBool is_minimal_non_free(const cbitset &);

        // The argument is a column set describing a submatroid.
        // Return:
        // - True if the argument is inductively free. 
        // - False if the characteristic polynomial does not split, or there is
        //   a corank one flat whose characteristic polynomial does not split.
        // - Uncertain if the submatroid is not inductively free, but the characteristic
        //   polynomial of the matroid and all corank one flats does split.
        // This function should be used in preference to is_free (for
        // submatroids) if it is important to eliminate as many Uncertain
        // answers as possible. 
        FuzzyBool is_free_submatroid(const cbitset &);
};

std::ostream &operator<< (std::ostream &, const Matroid::cbitset &);
std::ostream &operator<< (std::ostream &, const Matroid::Minor &);
std::ostream &operator<< (std::ostream &, const Matroid::ExponentGuess &);
std::ostream &operator<< (std::ostream &, const Matroid::FuzzyBool &);

Matroid::FuzzyBool operator&& (const Matroid::FuzzyBool&, const Matroid::FuzzyBool &);
Matroid::FuzzyBool operator! (const Matroid::FuzzyBool &);
