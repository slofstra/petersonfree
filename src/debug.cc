#include <iostream>
#include "matroid.h"
#include "debug.h"
using namespace std;

// A function which prints a period to the screen to track progress
int step=0;
void print_keepalive()
{   
    if (KEEPALIVE) {
        step++;
        if (step %10000 == 0) cout << ".";
    }
}

unsigned debug_tab=0;
// keep tabs aligned for printing
void print_tab() {
    //for (unsigned i=0; i<debug_tab; i++) cout << "\t";
    for (unsigned i=0; i<debug_tab; i++) cout << "    ";
}
